# Running as www-data to read /etc/roundcube/config.inc.php and /etc/roundcube/debian-db.php
Features: test-name=upstream-testsuite
Test-Command: mkdir -p tests/.locale &&
 localedef -c -i en_US -f UTF-8 tests/.locale/en_US.utf8 &&
 /sbin/runuser -u www-data -- env RCUBE_INSTALL_PATH=/var/lib/roundcube LOCPATH="$(pwd)/tests/.locale"
  phpunit -c tests/phpunit.xml --exclude-group=flaky
# WARN: roundcube-sqlite3 needs to appear *before* roundcube-core as otherwise it attempts to configure -mysql
Depends: aspell-en,
         hunspell-en-us,
         locales,
         php-bacon-qr-code [!armel !armhf !i386 !mipsel],
         php-crypt-gpg,
         php-enchant,
         php-gd,
         php-net-ldap3,
         php-pspell,
         php-roundcube-rtf-html-php,
         phpunit,
         roundcube-sqlite3,
         roundcube-core,
         roundcube-plugins
# XXX for better coverage (Browser tests) we would need Laravel Dusk, see .github/run.sh and tests/Browser/README.md
Restrictions: superficial, needs-root, rw-build-tree

Tests: check-upstream-version-number
Depends: roundcube-sqlite3, roundcube-core
Restrictions: superficial

# out of the box deployment with default HTTPd
Tests: apache2, cleanup, installer-checks
Depends: apache2, default-mysql-server, roundcube, curl
Restrictions: breaks-testbed, isolation-container, needs-root

# out of the box deployment with default lighttpd, both with php-fpm and
# php-cgi (prexisting fastcgi .php handler)
Tests: lighttpd
Depends: lighttpd (>= 1.4.55-2),
         default-mysql-server,
         php-cgi,
         php-fpm,
         roundcube,
         curl
Restrictions: breaks-testbed, isolation-container, needs-root

# check that ownership and permissions on configuration files and
# log/temp directories are sane; check also privilege drop for
# bin/update.sh
Tests: config-ownership-perms
Depends: roundcube-sqlite3, roundcube-core
Restrictions: allow-stderr, breaks-testbed, needs-root

# an hardened configuration with a dedicated PHP-FPM pool and dedicated
# user/group (so the HTTPd can't read sensitive roundcube data)
Tests: hardening-dedicated-user
Depends: nginx-light,
         default-mysql-server,
         php-fpm,
         roundcube,
         curl
Restrictions: needs-root, isolation-container

# dbconfig-no-thanks: set custom $config['db_dsnw']
Tests: dbconfig-no-thanks
Depends: apache2, dbconfig-no-thanks, roundcube-sqlite3, roundcube-core, curl
Restrictions: breaks-testbed, isolation-container, needs-root
